<?php

namespace App\Tests\Unit\insertCoin;


use App\Tests\Repository\InlineBudgetRepository;
use App\VendingMachine\Domain\Service\InsertCoin\InsertCoinCommand;
use App\VendingMachine\Domain\Service\InsertCoin\InsertCoinCommandHandler;
use PHPUnit\Framework\TestCase;

class InsertCoinTest extends TestCase
{
    /**
     * @test
     * @dataProvider validCoinsDataProvider
     */
    public function givenAValidCoinThenAddToTheAmount(
        float $value
    ) {
        $repository = new InlineBudgetRepository();
        $useCase = new InsertCoinCommandHandler($repository);

        $useCase(new InsertCoinCommand($value));
        $this->assertCount(1, $repository->coins);
    }

    /**
     * @test
     * @dataProvider invalidCoinDataProvider
     */
    public function givenAnInvalidCoinThenThrowsAnException(
        float $value
    ) {
        $repository = new InlineBudgetRepository();
        $useCase = new InsertCoinCommandHandler($repository);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Coin not allowed');
        $useCase(new InsertCoinCommand($value));
    }

    public function validCoinsDataProvider()
    {
        return [
            [0.05],
            [0.10],
            [0.25],
            [1.0]
        ];
    }

    public function invalidCoinDataProvider()
    {
        return [
            [0.50],
            [0.01],
            [0.26],
            [2.0]
        ];
    }
}
