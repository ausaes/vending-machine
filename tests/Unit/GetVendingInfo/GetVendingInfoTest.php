<?php

namespace App\Tests\Unit\GetVendingInfo;

use App\VendingMachine\Domain\Repository\CoinRepository;
use App\VendingMachine\Domain\Repository\StockRepository;
use App\VendingMachine\Domain\Service\GetVendingInfo\GetVendingInfoQuery;
use App\VendingMachine\Domain\Service\GetVendingInfo\GetVendingInfoQueryHandler;
use App\VendingMachine\Domain\Service\GetVendingInfo\GetVendingInfoResponse;
use App\VendingMachine\Domain\ValueObject\Coin;
use App\VendingMachine\Domain\ValueObject\CoinLine;
use App\VendingMachine\Domain\ValueObject\Inventory;
use App\VendingMachine\Domain\ValueObject\ProductLine;
use App\VendingMachine\Domain\ValueObject\Product;
use App\VendingMachine\Domain\ValueObject\Wallet;
use PHPUnit\Framework\TestCase;

class GetVendingInfoTest extends TestCase
{
    /** @test */
    public function theServiceShouldReturnAllTheInfo()
    {
        $coinRepository = $this->getMockBuilder(CoinRepository::class)->getMock();
        $stockRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $stockRepository->method('getInventory')
            ->willReturn(
                new Inventory([
                    new ProductLine(new Product('Water', 0.65), 10)
                ])
            );

        $coinRepository->method('getWallet')
            ->willReturn(
                new Wallet(
                    [
                        new CoinLine(Coin::createFromValue(0.25), 100)
                    ]
                )
            );
        $handler = new GetVendingInfoQueryHandler($coinRepository, $stockRepository);
        /** @var GetVendingInfoResponse $result */
        $result = $handler(new GetVendingInfoQuery());
        $this->assertCount(1, $result->getInventory()->getProductLines());
        $this->assertCount(1, $result->getWallet()->getCoinLines());
    }
}
