<?php

namespace App\Tests\Unit\GetProduct;

use App\VendingMachine\Domain\Repository\BudgetRepository;
use App\VendingMachine\Domain\Repository\CoinRepository;
use App\VendingMachine\Domain\Repository\StockRepository;
use App\VendingMachine\Domain\Service\GetProduct\GetInventoryProductQuery;
use App\VendingMachine\Domain\Service\GetProduct\GetInventoryProductQueryHandler;
use App\VendingMachine\Domain\Service\GetProduct\GetInventoryProductResponse;
use App\VendingMachine\Domain\ValueObject\Coin;
use App\VendingMachine\Domain\ValueObject\ProductLine;
use App\VendingMachine\Domain\ValueObject\Product;
use PHPUnit\Framework\TestCase;

class GetProductTest extends TestCase
{
    /**
     * @test
     */
    public function givenALessAmountThanRequiredThenTheMachineDontReturnTheProduct()
    {
        $coinRepository = $this->getMockBuilder(CoinRepository::class)->getMock();
        $stockRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $stockRepository->method('checkInventoryByName')
            ->willReturn(new ProductLine(new Product('Water', 0.65), 100));

        $budgetRepository = $this->getMockBuilder(BudgetRepository::class)->getMock();
        $budgetRepository->method('getBudgetAmount')->willReturn(0.64);
        $handler = new GetInventoryProductQueryHandler($stockRepository, $budgetRepository, $coinRepository);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Not enough money');
        $handler(new GetInventoryProductQuery(1));
    }

    /** @test */
    public function givenAOutOfStockProductThenTheMachineDoesNotReturnAnything()
    {
        $coinRepository = $this->getMockBuilder(CoinRepository::class)->getMock();

        $stockRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $stockRepository->method('checkInventoryByName')
            ->willReturn(new ProductLine(new Product('Water', 0.65), 0));


        $budgetRepository = $this->getMockBuilder(BudgetRepository::class)->getMock();
        $useCase = new GetInventoryProductQueryHandler($stockRepository, $budgetRepository, $coinRepository);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Out of stock');
        $useCase(new GetInventoryProductQuery(1));
    }

    /** @test */
    public function givenAValidProductAndWeHaveExactBudgetThenTheMachineReturnsTheProduct()
    {
        $coinRepository = $this->getMockBuilder(CoinRepository::class)->getMock();

        $stockRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $stockRepository->method('checkInventoryByName')
            ->willReturn(new ProductLine(new Product('Water', 0.65), 100));

        $budgetRepository = $this->getMockBuilder(BudgetRepository::class)->getMock();
        $budgetRepository->method('getBudgetAmount')->willReturn(0.65);
        $handler = new GetInventoryProductQueryHandler($stockRepository, $budgetRepository, $coinRepository);

        $stockRepository
            ->expects($this->once())
            ->method('buyProductByName')
            ->with($this->equalTo(1));

        $budgetRepository
            ->expects($this->once())
            ->method('emptyBudget');


        /** @var GetInventoryProductResponse $response */
        $response = $handler(new GetInventoryProductQuery(1));
        $this->assertEmpty($response->getChange());
        $this->assertEquals('Water',$response->getInventory()->getProduct()->getName());
    }

    /** @test */
    public function givenAValidProductAndWeHaveExtraBudgetThenTheMachineReturnsTheProduct()
    {
        $coinRepository = $this->getMockBuilder(CoinRepository::class)->getMock();
        $coinRepository->method('returnChange')
            ->willReturn(
                [
                    Coin::createFromValue('0.25'),
                    Coin::createFromValue('0.10')
                ]
            );
        $stockRepository = $this->getMockBuilder(StockRepository::class)->getMock();
        $stockRepository->method('checkInventoryByName')
            ->willReturn(new ProductLine(new Product('Water', 0.65), 100));


        $budgetRepository = $this->getMockBuilder(BudgetRepository::class)->getMock();
        $budgetRepository->method('getBudgetAmount')->willReturn(1.0);
        $handler = new GetInventoryProductQueryHandler($stockRepository, $budgetRepository, $coinRepository);

        $stockRepository
            ->expects($this->once())
            ->method('buyProductByName')
            ->with($this->equalTo(1));

        $budgetRepository
            ->expects($this->once())
            ->method('emptyBudget');


        /** @var GetInventoryProductResponse $response */
        $response = $handler(new GetInventoryProductQuery(1));
        $this->assertCount(2, $response->getChange());
    }
}
