<?php

namespace App\Tests\ReturnCoins;

use App\VendingMachine\Domain\Repository\BudgetRepository;
use App\VendingMachine\Domain\Service\ReturnCoins\ReturnCoinsCommandHandler;
use PHPUnit\Framework\TestCase;

class ReturnCoinsTest extends TestCase
{
    /**
     * @test
     */
    public function whenTheUserWantsToReturnCoinsThenTheMachineDoesIt()
    {
        $repository = $this->getMockBuilder(BudgetRepository::class)->getMock();

        $useCase = new ReturnCoinsCommandHandler($repository);

        $repository
            ->expects($this->once())
            ->method('emptyBudget');
        $useCase();
    }
}
