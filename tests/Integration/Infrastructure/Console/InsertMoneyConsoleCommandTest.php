<?php

namespace App\Tests\Integration\Infrastructure\Console;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class InsertMoneyConsoleCommandTest extends KernelTestCase
{
    private const BUDGET_DATABASE = '/app/tests/database/budget.json';

    private string $budgetFile;

    public function setUp(): void
    {
        $this->budgetFile = file_get_contents(self::BUDGET_DATABASE);
    }

    protected function tearDown(): void
    {
        $file = fopen(self::BUDGET_DATABASE, 'w');
        fwrite($file, $this->budgetFile);
        fclose($file);
    }

    /** @test */
    public function givenAnInvalidTypeOfCoinThenItShowsTheError()
    {
        $commandTester = $this->getCommand();
        $commandTester->execute(['coin' => 0.3]);
        $output = $commandTester->getDisplay();
        $this->assertEquals("Coin not allowed\n", $output);
        $this->assertEquals(1, $commandTester->getStatusCode());

    }

    /** @test */
    public function givenAValidTypeOfCoinThenItInsert()
    {
        $commandTester = $this->getCommand();
        $commandTester->execute(['coin' => 0.25]);
        $output = $commandTester->getDisplay();
        $this->assertEquals('', $output);
        $this->assertEquals(0, $commandTester->getStatusCode());
    }

    private function getCommand(): CommandTester
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('vending:insert-money');
        return new CommandTester($command);
    }
}
