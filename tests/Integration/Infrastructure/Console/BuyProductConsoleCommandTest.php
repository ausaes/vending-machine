<?php

namespace App\Tests\Integration\Infrastructure\Console;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class BuyProductConsoleCommandTest extends KernelTestCase
{
    private const COIN_DATABASE = '/app/tests/database/coin.json';
    private const STOCK_DATABASE = '/app/tests/database/stock.json';
    private const BUDGET_DATABASE = '/app/tests/database/budget.json';

    private string $stockFile;

    private string $coinFile;

    private string $budgetFile;

    public function setUp(): void
    {
        $this->stockFile = file_get_contents(self::STOCK_DATABASE);
        $this->coinFile = file_get_contents(self::COIN_DATABASE);
        $this->budgetFile = file_get_contents(self::BUDGET_DATABASE);
    }

    protected function tearDown(): void
    {
        $this->initializeDatabases();
    }

    /** @test */
    public function givenAnInvalidProductNameThenItShowsTheError()
    {
        $command = $this->getBuyProductCommand();
        $command->execute(['name' => 'invalid']);
        $this->assertEquals("Invalid product\n", $command->getDisplay());
        $this->assertEquals(1, $command->getStatusCode());
    }

    /** @test */
    public function givenAValidProductNameThenItShowsTheErrorWhenTheBudgetIsNotEnough()
    {
        $command = $this->getBuyProductCommand();
        $command->execute(['name' => 'Water']);
        $this->assertEquals("Not enough money\n", $command->getDisplay());
        $this->assertEquals(1, $command->getStatusCode());
    }

    /** @test */
    public function givenAValidProductNameAndExactBudgetThenItShowsTheFeedback()
    {
        $insertCoinCommand = $this->getInsertCoinCommand();
        $insertCoinCommand->execute(['coin' => 0.25]);
        $insertCoinCommand->execute(['coin' => 0.25]);
        $insertCoinCommand->execute(['coin' => 0.1]);
        $insertCoinCommand->execute(['coin' => 0.05]);
        $buyProductCommand = $this->getBuyProductCommand();
        $buyProductCommand->execute(['name' => 'Water']);
        $this->assertEquals("Water \n", $buyProductCommand->getDisplay());
        $this->assertEquals(0, $buyProductCommand->getStatusCode());
    }

    /** @test */
    public function givenAValidProductNameAndExtraBudgetThenItShowsTheFeedback()
    {
        $insertCoinCommand = $this->getInsertCoinCommand();
        $insertCoinCommand->execute(['coin' => 1]);
        $buyProductCommand = $this->getBuyProductCommand();
        $buyProductCommand->execute(['name' => 'Water']);
        $this->assertEquals("Water 0.25, 0.1\n", $buyProductCommand->getDisplay());
        $this->assertEquals(0, $buyProductCommand->getStatusCode());
    }

    private function getInsertCoinCommand(): CommandTester
    {
        return $this->getCommand('vending:insert-money');
    }

    private function getBuyProductCommand(): CommandTester
    {
        return $this->getCommand('vending:buy-product');
    }

    private function getCommand(string $commandName)
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find($commandName);
        return new CommandTester($command);
    }

    private function initializeDatabases(): void
    {
        $file = fopen(self::STOCK_DATABASE, 'w');
        fwrite($file, $this->stockFile);
        fclose($file);
        $file = fopen(self::COIN_DATABASE, 'w');
        fwrite($file, $this->coinFile);
        fclose($file);
        $file = fopen(self::BUDGET_DATABASE, 'w');
        fwrite($file, $this->budgetFile);
        fclose($file);
    }
}
