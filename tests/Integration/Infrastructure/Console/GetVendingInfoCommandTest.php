<?php

namespace App\Tests\Integration\Infrastructure\Console;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class GetVendingInfoCommandTest extends KernelTestCase
{
    /** @test */
    public function theCommandMustShowTheInfo()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('vending:check-machine');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);
        $this->assertEquals(0, $commandTester->getStatusCode());
        $this->assertStringContainsString("Wallet", $commandTester->getDisplay());
        $this->assertStringContainsString("Value: 0.1	 quantity:100", $commandTester->getDisplay());
        $this->assertStringContainsString("Value: 0.25	 quantity:100", $commandTester->getDisplay());
        $this->assertStringContainsString("Value: 0.05	 quantity:100", $commandTester->getDisplay());
        $this->assertStringContainsString("Value: 1	 quantity:1", $commandTester->getDisplay());

        $this->assertStringContainsString('Inventory', $commandTester->getDisplay());
        $this->assertStringContainsString('Water	Price: 0.65	Quantity: 100', $commandTester->getDisplay());
        $this->assertStringContainsString('Soda	Price: 0.5	Quantity: 0', $commandTester->getDisplay());

    }

}
