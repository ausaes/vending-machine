<?php

namespace App\Tests\Integration\Infrastructure\Console;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class ReturnCoinsConsoleCommandTest extends KernelTestCase
{
    /** @test */
    public function itShouldShowTheFeedback()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('vending:return-coins');
        $commandTester = new CommandTester($command);
        $commandTester->execute([]);
        $output = $commandTester->getDisplay();
        $this->assertEquals("coins returned\n", $output);
        $this->assertEquals(0, $commandTester->getStatusCode());
    }
}
