<?php

namespace App\Tests\Integration\Infrastructure\Repository;

use App\VendingMachine\Domain\ValueObject\ProductLine;
use App\VendingMachine\Domain\ValueObject\Product;
use App\VendingMachine\Infrastructure\Repository\FileStockRepository;
use PHPUnit\Framework\TestCase;

class FileStockRepositoryTest extends TestCase
{
    private FileStockRepository $repository;

    private string $file;
    private string $fileName;

    public function setUp(): void
    {
        $this->fileName = '/app/tests/database/stock.json';
        $this->repository = new FileStockRepository($this->fileName);
        $this->file = file_get_contents($this->fileName);
    }
    protected function tearDown(): void
    {
        $file = fopen($this->fileName, 'w');
        fwrite($file, $this->file);
        fclose($file);
    }

    /** @test */
    public function givenAValidNameThenItReturnsAnInventory()
    {
        $inventory = $this->repository->checkInventoryByName('Water');
        $this->assertEquals(
            new ProductLine(new Product('Water', 0.65), 100),
            $inventory
        );
    }
    /** @test */
    public function givenAnInvalidNameThenItReturnsAnException()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Invalid product');
        $this->repository->checkInventoryByName('Invalid');
    }

    /** @test */
    public function givenAValidNameThenItManageAllTheInventoryStuffWhenWeBuyIt()
    {
        $beforeBuyInventory = $this->repository->checkInventoryByName('Water');
        $this->repository->buyProductByName('Water');
        $afterBuyInventory = $this->repository->checkInventoryByName('Water');
        $this->assertEquals(100, $beforeBuyInventory->getQuantity());
        $this->assertEquals(99, $afterBuyInventory->getQuantity());
    }
    /** @test */
    public function givenAValidNameButSoldOutProductThenItReturnsAnError()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Out of stock');
        $this->repository->buyProductByName('Soda');
    }

    /** @test */
    public function itShouldReturnAllStock()
    {
        $stock = $this->repository->getInventory();
        $this->assertCount(2, $stock->getProductLines());
        $this->assertEquals(100, $stock->getProductLines()[0]->getQuantity());
        $this->repository->buyProductByName('Water');
        $this->assertEquals(
            99,
            $this->repository->getInventory()->getProductLines()[0]->getQuantity()
        );
    }
}
