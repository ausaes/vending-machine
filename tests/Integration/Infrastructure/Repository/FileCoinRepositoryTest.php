<?php

namespace App\Tests\Integration\Infrastructure\Repository;

use App\VendingMachine\Domain\ValueObject\Coin;
use App\VendingMachine\Infrastructure\Repository\FileCoinRepository;
use PHPUnit\Framework\TestCase;

class FileCoinRepositoryTest extends TestCase
{
    private FileCoinRepository $repository;
    private string $file;
    private string $fileName;

    public function setUp(): void
    {
        $this->fileName = '/app/tests/database/coin.json';
        $this->repository = new FileCoinRepository($this->fileName);
        $this->file = file_get_contents($this->fileName);
    }

    protected function tearDown(): void
    {
        $file = fopen($this->fileName, 'w');
        fwrite($file, $this->file);
        fclose($file);
    }

    /** @test */
    public function givenAChangeAmountThenItReturnsAllTheCoins()
    {
        $coins = $this->repository->returnChange(0.35);
        $this->assertCount(2, $coins);
        $this->assertEquals(Coin::createFromValue(0.25), $coins[0]);
        $this->assertEquals(Coin::createFromValue(0.10), $coins[1]);
    }

    /** @test */
    public function giveAChangeAmountThenItReturnsAnErrorIfItCanNotReturnCoins()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Can\'t return coin');
        $this->repository->returnChange(0.02);
    }
    /** @test */
    public function givenAChangeAmountThenItReturnsTheCoinsRespectingTheQuantity()
    {
        $coins = $this->repository->returnChange(2);
        $this->assertCount(5, $coins);
        $this->assertEquals(Coin::createFromValue(1), $coins[0]);
        $this->assertEquals(Coin::createFromValue(0.25), $coins[1]);
        $this->assertEquals(Coin::createFromValue(0.25), $coins[2]);
        $this->assertEquals(Coin::createFromValue(0.25), $coins[3]);
        $this->assertEquals(Coin::createFromValue(0.25), $coins[4]);
    }

    /** @test */
    public function givenAEmptyChangeAmountThenItDoesNotReturnCoins()
    {
        $coins = $this->repository->returnChange(0);
        $this->assertCount(0, $coins);
    }
    /** @test */
    public function itShouldReturnAllCoins()
    {
        $wallet = $this->repository->getWallet();
        $this->assertCount(4, $wallet->getCoinLines());
        $this->assertEquals(100, $wallet->getCoinLines()[0]->getQuantity());
        $this->assertEquals(0.1, $wallet->getCoinLines()[0]->getCoin()->getValue());
        $this->repository->returnChange(0.1);
        $this->assertEquals(
            99,
            $this->repository->getWallet()->getCoinLines()[0]->getQuantity()
        );
    }
}
