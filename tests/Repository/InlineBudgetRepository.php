<?php

namespace App\Tests\Repository;

use App\VendingMachine\Domain\Repository\BudgetRepository;
use App\VendingMachine\Domain\ValueObject\Coin;

class InlineBudgetRepository implements BudgetRepository {

    public array $coins = [];
    public function addCoin(Coin $coin): void
    {
        $this->coins[] = $coin;
    }

    public function emptyBudget(): void
    {
        $this->coins = [];
    }

    public function getBudgetAmount(): float
    {
        return 0.0;
    }

    public function getCoins(): array
    {
        return $this->coins;
    }
}
