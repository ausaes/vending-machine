<?php

namespace App\VendingMachine\Domain\ValueObject;

class Coin
{
    private const ALLOWED_VALUES = [0.05, 0.10, 0.25, 1];
    private float $value;

    private function __construct(float $value)
    {
        $this->value = $value;
    }

    public static function createFromValue(float $value): self
    {
        if (!in_array($value, self::ALLOWED_VALUES)) {
            throw new \Exception('Coin not allowed');
        }
        return new self($value);
    }

    public function getValue(): float
    {
        return $this->value;
    }

}
