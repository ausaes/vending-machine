<?php

namespace App\VendingMachine\Domain\ValueObject;

class CoinLine
{
    private Coin $coin;
    private int $quantity;

    public function __construct(Coin $coin, int $quantity)
    {
        $this->coin = $coin;
        $this->quantity = $quantity;
    }

    public function getCoin(): Coin
    {
        return $this->coin;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

}
