<?php

namespace App\VendingMachine\Domain\ValueObject;

class Wallet
{
    /**
     * @var CoinLine[]
     */
    private array $coinLines;

    public function __construct($coinLines)
    {
        $this->coinLines = $coinLines;
    }

    public function getCoinLines(): array
    {
        return $this->coinLines;
    }

}
