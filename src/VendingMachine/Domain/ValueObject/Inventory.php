<?php

namespace App\VendingMachine\Domain\ValueObject;

class Inventory
{
    /**
     * @var ProductLine[]
     */
    private array $productLines;

    public function __construct($productLines)
    {
        $this->productLines = $productLines;
    }

    public function getProductLines(): array
    {
        return $this->productLines;
    }

}
