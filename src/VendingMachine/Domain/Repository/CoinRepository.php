<?php

namespace App\VendingMachine\Domain\Repository;

use App\VendingMachine\Domain\ValueObject\Coin;
use App\VendingMachine\Domain\ValueObject\CoinLine;
use App\VendingMachine\Domain\ValueObject\Wallet;

interface CoinRepository
{
    /**
     * @param float $change
     * @return Coin[]
     */
    public function returnChange(float $change): array;

    /**
     * @return Wallet
     */
    public function getWallet(): Wallet;
}