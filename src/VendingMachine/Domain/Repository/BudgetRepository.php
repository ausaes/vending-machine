<?php

namespace App\VendingMachine\Domain\Repository;

use App\VendingMachine\Domain\ValueObject\Coin;

interface BudgetRepository
{
    public function addCoin(Coin $coin): void;

    public function getCoins(): array;

    public function emptyBudget(): void;

    public function getBudgetAmount(): float;
}
