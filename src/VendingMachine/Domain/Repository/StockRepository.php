<?php

namespace App\VendingMachine\Domain\Repository;

use App\VendingMachine\Domain\ValueObject\Inventory;
use App\VendingMachine\Domain\ValueObject\ProductLine;
use App\VendingMachine\Domain\ValueObject\Product;

interface StockRepository
{
    public function checkInventoryByName(string $name): ProductLine;

    public function buyProductByName(string $name): Product;

    public function getInventory(): Inventory;
}