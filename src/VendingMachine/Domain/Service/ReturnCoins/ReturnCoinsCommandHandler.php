<?php

namespace App\VendingMachine\Domain\Service\ReturnCoins;

use App\VendingMachine\Domain\Bus\Command\CommandHandler;
use App\VendingMachine\Domain\Repository\BudgetRepository;

class ReturnCoinsCommandHandler implements CommandHandler
{
    private BudgetRepository $budgetRepository;

    public function __construct(
        BudgetRepository $budgetRepository
    ) {
        $this->budgetRepository = $budgetRepository;
    }

    public function __invoke(): void
    {
        $this->budgetRepository->emptyBudget();
    }

}
