<?php

namespace App\VendingMachine\Domain\Service\GetProduct;

use App\VendingMachine\Domain\Bus\Query\QueryHandler;
use App\VendingMachine\Domain\Bus\Query\Response;
use App\VendingMachine\Domain\Repository\BudgetRepository;
use App\VendingMachine\Domain\Repository\CoinRepository;
use App\VendingMachine\Domain\Repository\StockRepository;

class GetInventoryProductQueryHandler implements QueryHandler
{

    private StockRepository $stockRepository;

    private BudgetRepository $budgetRepository;
    /**
     * @var CoinRepository
     */
    private CoinRepository $coinRepository;

    public function __construct(
        StockRepository $stockRepository,
        BudgetRepository $budgetRepository,
        CoinRepository $coinRepository
    )
    {
        $this->stockRepository = $stockRepository;
        $this->budgetRepository = $budgetRepository;
        $this->coinRepository = $coinRepository;
    }

    public function __invoke(GetInventoryProductQuery $query): Response
    {
        $inventory = $this->stockRepository->checkInventoryByName($query->getName());
        if ($inventory->getQuantity() === 0) {
            throw new \Exception('Out of stock');
        }
        $budget = $this->budgetRepository->getBudgetAmount();
        $changeAmount = $budget - $inventory->getProduct()->getPrice();

        if($changeAmount < 0) {
            throw new \Exception('Not enough money');
        }
        $this->stockRepository->buyProductByName($query->getName());
        $changeCoins = $this->coinRepository->returnChange($changeAmount);
        $this->budgetRepository->emptyBudget();

        return new GetInventoryProductResponse(
            $this->stockRepository->checkInventoryByName($query->getName()),
            $changeCoins
        );
    }
}
