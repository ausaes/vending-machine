<?php

namespace App\VendingMachine\Domain\Service\GetProduct;

use App\VendingMachine\Domain\Bus\Query\Response;
use App\VendingMachine\Domain\ValueObject\ProductLine;

class GetInventoryProductResponse implements Response
{
    private ProductLine $inventory;
    private array $change;

    public function __construct(ProductLine $inventory, array $change)
    {
        $this->inventory = $inventory;
        $this->change = $change;
    }

    public function getInventory(): ProductLine
    {
        return $this->inventory;
    }

    public function getChange(): array
    {
        return $this->change;
    }


}
