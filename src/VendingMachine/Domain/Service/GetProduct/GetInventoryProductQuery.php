<?php

namespace App\VendingMachine\Domain\Service\GetProduct;

use App\VendingMachine\Domain\Bus\Query\Query;

class GetInventoryProductQuery implements Query
{
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
