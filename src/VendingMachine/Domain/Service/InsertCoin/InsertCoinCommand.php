<?php

namespace App\VendingMachine\Domain\Service\InsertCoin;

use App\VendingMachine\Domain\Bus\Command\Command;

class InsertCoinCommand implements Command
{
    private float $value;
    public function __construct(float $value)
    {
        $this->value = $value;
    }

    public function getValue(): float
    {
        return $this->value;
    }
}
