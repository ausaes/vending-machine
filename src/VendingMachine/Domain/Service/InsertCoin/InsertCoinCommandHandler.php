<?php

namespace App\VendingMachine\Domain\Service\InsertCoin;

use App\VendingMachine\Domain\Bus\Command\Command;
use App\VendingMachine\Domain\Bus\Command\CommandHandler;
use App\VendingMachine\Domain\Repository\BudgetRepository;
use App\VendingMachine\Domain\ValueObject\Coin;

final class InsertCoinCommandHandler implements CommandHandler
{
    private BudgetRepository $budgetRepository;

    public function __construct(BudgetRepository $budgetRepository)
    {
        $this->budgetRepository = $budgetRepository;
    }

    public function __invoke(Command $command): void
    {

        $coin = Coin::createFromValue($command->getValue());
        $this->budgetRepository->addCoin($coin);
    }
}
