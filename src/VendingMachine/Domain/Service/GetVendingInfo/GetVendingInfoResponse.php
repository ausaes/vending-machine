<?php

namespace App\VendingMachine\Domain\Service\GetVendingInfo;

use App\VendingMachine\Domain\Bus\Query\Response;
use App\VendingMachine\Domain\ValueObject\Inventory;
use App\VendingMachine\Domain\ValueObject\Wallet;

class GetVendingInfoResponse implements Response
{
    private Wallet $wallet;
    private Inventory $inventory;

    public function __construct(Wallet $wallet, Inventory $inventory)
    {
        $this->wallet = $wallet;
        $this->inventory = $inventory;
    }

    public function getWallet(): Wallet
    {
        return $this->wallet;
    }

    public function getInventory(): Inventory
    {
        return $this->inventory;
    }

}
