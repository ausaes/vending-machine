<?php

namespace App\VendingMachine\Domain\Service\GetVendingInfo;

use App\VendingMachine\Domain\Bus\Query\QueryHandler;
use App\VendingMachine\Domain\Bus\Query\Response;
use App\VendingMachine\Domain\Repository\CoinRepository;
use App\VendingMachine\Domain\Repository\StockRepository;

class GetVendingInfoQueryHandler implements QueryHandler
{

    private CoinRepository $coinRepository;
    private StockRepository $stockRepository;

    public function __construct(CoinRepository $coinRepository, StockRepository $stockRepository)
    {
        $this->coinRepository = $coinRepository;
        $this->stockRepository = $stockRepository;
    }

    public function __invoke(GetVendingInfoQuery $query): Response
    {
        return new GetVendingInfoResponse(
            $this->coinRepository->getWallet(),
            $this->stockRepository->getInventory()
        );
    }

}
