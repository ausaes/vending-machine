<?php

namespace App\VendingMachine\Domain\Bus\Query;

interface QueryBus
{
    public function ask(Query $query): ?Response;
}