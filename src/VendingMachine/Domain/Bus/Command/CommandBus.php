<?php

namespace App\VendingMachine\Domain\Bus\Command;


interface CommandBus
{
    public function dispatch(Command $command): void;
}
