<?php

namespace App\VendingMachine\Infrastructure\Bus;

use App\VendingMachine\Domain\Bus\Query\Query;
use App\VendingMachine\Domain\Bus\Query\QueryBus;
use App\VendingMachine\Domain\Bus\Query\Response;
use App\VendingMachine\Domain\Service\GetProduct\GetInventoryProductQuery;
use App\VendingMachine\Domain\Service\GetProduct\GetInventoryProductQueryHandler;
use App\VendingMachine\Domain\Service\GetVendingInfo\GetVendingInfoQuery;
use App\VendingMachine\Domain\Service\GetVendingInfo\GetVendingInfoQueryHandler;
use Symfony\Component\Messenger\Handler\HandlersLocator;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\Middleware\HandleMessageMiddleware;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class SymfonyQueryBus implements QueryBus
{
    private MessageBus $bus;

    public function __construct(
        GetInventoryProductQueryHandler $getInventoryProductQueryHandler,
        GetVendingInfoQueryHandler $getVendingInfoQueryHandler
    )
    {
        $this->bus = new MessageBus(
            [
                new HandleMessageMiddleware(
                    new HandlersLocator(
                        [
                            GetInventoryProductQuery::class => [$getInventoryProductQueryHandler],
                            GetVendingInfoQuery::class => [$getVendingInfoQueryHandler]
                        ]
                    )
                )
            ]
        );
    }

    public function ask(Query $query): ?Response
    {
        /** @var HandledStamp $stamp */
        $stamp = $this->bus->dispatch($query)->last(HandledStamp::class);
        return $stamp->getResult();
    }
}
