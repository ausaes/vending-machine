<?php

namespace App\VendingMachine\Infrastructure\Bus;

use App\VendingMachine\Domain\Bus\Command\Command;
use App\VendingMachine\Domain\Bus\Command\CommandBus;
use App\VendingMachine\Domain\Service\GetProduct\GetProductCommand;
use App\VendingMachine\Domain\Service\GetProduct\GetProductCommandHandler;
use App\VendingMachine\Domain\Service\InsertCoin\InsertCoinCommand;
use App\VendingMachine\Domain\Service\InsertCoin\InsertCoinCommandHandler;
use App\VendingMachine\Domain\Service\ReturnCoins\ReturnCoinsCommand;
use App\VendingMachine\Domain\Service\ReturnCoins\ReturnCoinsCommandHandler;
use Symfony\Component\Messenger\Handler\HandlersLocator;
use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Messenger\Middleware\HandleMessageMiddleware;

class SymfonyCommandBus implements CommandBus
{

    private MessageBus $bus;

    public function __construct(
        InsertCoinCommandHandler $insertCoinCommandHandler,
        ReturnCoinsCommandHandler $returnCoinsCommandHandler
    ) {
        $this->bus = new MessageBus(
            [
                new HandleMessageMiddleware(
                    new HandlersLocator([
                        InsertCoinCommand::class => [$insertCoinCommandHandler],
                        ReturnCoinsCommand::class => [$returnCoinsCommandHandler]
                    ])
                )
            ]
        );
    }

    public function dispatch(Command $command): void
    {
        $this->bus->dispatch($command);
    }
}
