<?php

namespace App\VendingMachine\Infrastructure\Repository;

use App\VendingMachine\Domain\Repository\StockRepository;
use App\VendingMachine\Domain\ValueObject\Inventory;
use App\VendingMachine\Domain\ValueObject\ProductLine;
use App\VendingMachine\Domain\ValueObject\Product;

class FileStockRepository implements StockRepository
{
    private string $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    public function checkInventoryByName(string $name): ProductLine
    {
        $content = $this->getContent();
        if (!isset($content[$name])) {
            throw new \Exception('Invalid product');
        }
        return $content[$name];
    }

    public function buyProductByName(string $name): Product
    {
        $inventory = $this->checkInventoryByName($name);
        if($inventory->getQuantity() === 0) {
            throw new \Exception('Out of stock');
        }
        $inventory = new ProductLine($inventory->getProduct(), $inventory->getQuantity() - 1);

        $this->updateInventory($name, $inventory);
        return $inventory->getProduct();
    }

    public function getInventory(): Inventory
    {
        $content = $this->getContent();
        $productLines = [];
        foreach($content as  $productLine) {
            $productLines[] = $productLine;
        }
        return new Inventory($productLines);
    }

    private function getContent(): array
    {
        $string = file_get_contents($this->filename);
        $content = [];
        foreach (json_decode($string, true) as $name => $details) {
            $content[$name] = new ProductLine(
                new Product($name, $details['price']), $details['quantity']
            );
        }
        return $content;
    }

    private function updateInventory(string $name, ProductLine $inventory)
    {
        $content = $this->getContent();
        $content[$name] = $inventory;
        $this->saveInFile($content);
    }

    private function saveInFile(array $content)
    {
        $json = [];
        /**
         * @var int $position
         * @var ProductLine $inventory
         */
        foreach ($content as $name => $inventory) {
            $json[$name] = [
                'quantity' => $inventory->getQuantity(),
                'price' => $inventory->getProduct()->getPrice()
            ];
        }
        $file = fopen($this->filename, 'w');
        fwrite($file, json_encode($json));
    }
}
