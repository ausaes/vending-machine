<?php

namespace App\VendingMachine\Infrastructure\Repository;

use App\VendingMachine\Domain\Repository\BudgetRepository;
use App\VendingMachine\Domain\ValueObject\Coin;

class FileBudgetRepository implements BudgetRepository
{

    private string $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    public function addCoin(Coin $coin): void
    {
        $content = $this->getFileContent();
        $content['coins'][] = $coin->getValue();

        $file = fopen($this->filename, 'w');
        fwrite($file, json_encode($content));
    }

    public function emptyBudget(): void
    {
        $content = $this->getFileContent();
        $content['coins'] = [];
        $file = fopen($this->filename, 'w');
        fwrite($file, json_encode($content));
    }

    public function getBudgetAmount(): float
    {
        $coins = $this->getCoins();
        $amount = 0.0;
        foreach($coins as $coin) {
            $amount += $coin->getValue();
        }
        return $amount;
    }

    public function getCoins(): array
    {
        $content = $this->getFileContent();
        $coins = [];
        foreach($content['coins'] as $rawCoin)
        {
            $coins[] = Coin::createFromValue((float)$rawCoin);
        }
        return $coins;
    }

    private function getFileContent(): array
    {
        return json_decode(file_get_contents($this->filename), true);
    }
}
