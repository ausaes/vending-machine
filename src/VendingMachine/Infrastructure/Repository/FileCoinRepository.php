<?php

namespace App\VendingMachine\Infrastructure\Repository;

use App\VendingMachine\Domain\Repository\CoinRepository;
use App\VendingMachine\Domain\ValueObject\Coin;
use App\VendingMachine\Domain\ValueObject\CoinLine;
use App\VendingMachine\Domain\ValueObject\Wallet;

class FileCoinRepository implements CoinRepository
{
    private string $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    public function returnChange(float $change): array
    {
        $coinsReturned = [];
        while ($this->getCoinsAmount($coinsReturned) < $change) {
            $coinsReturned[] = $this->addCoinToChange($change - $this->getCoinsAmount($coinsReturned));
        };

        return $coinsReturned;
    }

    public function getWallet(): Wallet
    {
        $fileWallet = $this->getFileWallet();
        $coinLines = [];
        foreach($fileWallet as $coinValue => $quantity)
        {
            $coinLines[] = new CoinLine(Coin::createFromValue($coinValue), $quantity);
        }
        return new Wallet($coinLines);
    }

    private function getFileWallet(): array
    {
        return json_decode(file_get_contents($this->filename), true);
    }

    private function setWallet(array $wallet): void
    {
        $file = fopen($this->filename, 'w');
        fwrite($file, json_encode($wallet));
    }

    private function addCoinToChange(float $changeToReturn): Coin
    {
        $coin = $this->findCoin($changeToReturn);
        $wallet = $this->getFileWallet();
        $wallet[(string)$coin->getValue()]--;
        $this->setWallet($wallet);
        return $coin;
    }

    private function getCoinsAmount(array $coins): float
    {
        $amount = 0.0;
        foreach ($coins as $coin) {
            $amount += $coin->getValue();
        }
        return $amount;
    }

    private function findCoin(float $changeToReturn): Coin
    {
        $wallet = $this->getFileWallet();
        krsort($wallet);
        foreach ($wallet as $value => $quantity) {
            if ($quantity === 0) {
                continue;
            }

            if (round($value, 2) <= round($changeToReturn, 2)) {
                return Coin::createFromValue($value);
            }
        }
        throw new \Exception('Can\'t return coin');
    }
}
