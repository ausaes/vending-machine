<?php

namespace App\VendingMachine\Infrastructure\Console;

use App\VendingMachine\Domain\Bus\Command\CommandBus;
use App\VendingMachine\Domain\Service\ReturnCoins\ReturnCoinsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReturnCoinsConsoleCommand extends Command
{
    protected static $defaultName = 'vending:return-coins';

    private CommandBus $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('return all inserted coins');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->commandBus->dispatch(new ReturnCoinsCommand());
        $output->writeln('coins returned');
        return 0;
    }
}
