<?php

namespace App\VendingMachine\Infrastructure\Console;

use App\VendingMachine\Domain\Bus\Command\CommandBus;
use App\VendingMachine\Domain\Bus\Query\QueryBus;
use App\VendingMachine\Domain\Service\GetProduct\GetInventoryProductQuery;
use App\VendingMachine\Domain\Service\GetProduct\GetInventoryProductResponse;
use App\VendingMachine\Domain\ValueObject\Coin;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BuyProductConsoleCommand extends Command
{

    protected static $defaultName = 'vending:buy-product';
    private const NAME = 'name';

    private CommandBus $commandBus;

    private QueryBus $queryBus;

    public function __construct(
        CommandBus $commandBus,
        QueryBus $queryBus
    ) {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('insert money to vending machine')
            ->addArgument(self::NAME, InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            /** @var GetInventoryProductResponse $response */
            $name = $input->getArgument(self::NAME);
            $response = $this->queryBus->ask(new GetInventoryProductQuery($name));
            $output->writeln(
                $response->getInventory()->getProduct()->getName() . " " .
                $this->mergeCoins($response->getChange())
            );
            return 0;
        } catch (\Exception $exception) {
            $output->writeln($exception->getMessage());
            return 1;
        }
    }

    private function mergeCoins(array $getChange)
    {
        return implode(', ', array_map(function (Coin $coin) {
            return $coin->getValue();
        }, $getChange));
    }
}
