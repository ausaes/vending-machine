<?php

namespace App\VendingMachine\Infrastructure\Console;

use App\VendingMachine\Domain\Bus\Query\QueryBus;
use App\VendingMachine\Domain\Service\GetVendingInfo\GetVendingInfoQuery;
use App\VendingMachine\Domain\Service\GetVendingInfo\GetVendingInfoResponse;
use App\VendingMachine\Domain\ValueObject\Inventory;
use App\VendingMachine\Domain\ValueObject\Wallet;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetVendingInfoCommand extends Command
{
    protected static $defaultName = 'vending:check-machine';

    private QueryBus $queryBus;

    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var GetVendingInfoResponse $response */
        $response = $this->queryBus->ask(new GetVendingInfoQuery());
        $this->showWalletInfo($output, $response->getWallet());
        $this->showInventoryInfo($output, $response->getInventory());
        return 0;
    }

    private function showWalletInfo(OutputInterface $output, Wallet $getWallet)
    {
        $output->writeln('Wallet');
        foreach($getWallet->getCoinLines() as $coinLine)
        {
            $output->writeln('Value: ' . $coinLine->getCoin()->getValue() . "\t quantity:" . $coinLine->getQuantity());
        }
        $output->writeln('');
    }

    private function showInventoryInfo(OutputInterface $output, Inventory $getInventory)
    {
        $output->writeln('Inventory');
        foreach($getInventory->getProductLines() as $productLine)
        {
            $output->writeln(
                $productLine->getProduct()->getName()
                . "\tPrice: " . $productLine->getProduct()->getPrice()
                . "\tQuantity: " . $productLine->getQuantity()
            );
        }
    }
}
