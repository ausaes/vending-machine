<?php

namespace App\VendingMachine\Infrastructure\Console;

use App\VendingMachine\Domain\Bus\Command\CommandBus;
use App\VendingMachine\Domain\Service\InsertCoin\InsertCoinCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InsertMoneyConsoleCommand extends Command
{
    protected static $defaultName = 'vending:insert-money';
    private const COIN = 'coin';

    private CommandBus $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('insert money to vending machine')
            ->addArgument(self::COIN, InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->commandBus->dispatch(new InsertCoinCommand($input->getArgument(self::COIN)));
            return 0;
        } catch (\Exception $exception) {
            $output->writeln($exception->getMessage());
            return 1;
        }
    }
}
